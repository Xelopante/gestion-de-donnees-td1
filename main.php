<?php
//Initialisation de l'affichae des erreurs
ini_set('display_errors', '1');

//Require des autoloader
require_once('vendor/autoload.php');
require_once('src/mf/utils/AbstractClassLoader.php');
require_once('src/mf/utils/ClassLoader.php');

$loader = new mf\utils\ClassLoader('src');
$loader->register();

//Alias du manager de l'ORM Eloquent
use Illuminate\Database\Capsule\Manager as EloquentManager;

//Configuration de la base pour Eloquent
$config = parse_ini_file("conf/config.ini");

$eloquent = new EloquentManager();
$eloquent->addConnection($config);
$eloquent->setAsGlobal();
$eloquent->bootEloquent();

//Déclaration des fonctions d'affichage
function afficherCarte($carte) {
    echo("Nom du propriétaire : ".$carte->nom_proprietaire."\r\n");
    echo("Mail du propriétaire : ".$carte->mail_proprietaire."\r\n");
    echo("Cumul : ".$carte->cumul."\r\n");
    echo("-----------------------------------------------------------------\r\n");
}

function afficherCartes($cartes) {
    foreach($cartes as $carte) {
        afficherCarte($carte);
    }
}

function afficherCommande($commande) {
    echo("ID de la commande : ".$commande->id."\r\n");
    echo("Date de livraison : ".$commande->date_livraison."\r\n");
    echo("Montant : ".$commande->montant."\r\n");
    if(isset($commande->etat)) {
        echo("Etat : ".$commande->etat."\r\n");
    }
    if(isset($commande->carte_id)) {
        echo("Etat : ".$commande->carte_id."\r\n");
    }
    echo("-----------------------------------------------------------------\r\n");
}

function afficherCommandes($commandes) {
    foreach($commandes as $commande) {
        afficherCommande($commande);
    }
}

function afficherItem($item) {
    echo("ID de l'item : ".$item->id."\r\n");
    echo("Libelle : ".$item->libelle."\r\n");
    echo("Description : ".$item->description."\r\n");
    echo("Tarif : ".$item->tarif."\r\n");
    if(isset($item->pivot)) {
        echo("Quantite : ".$item->pivot->quantite."\r\n");
    }
    echo("-----------------------------------------------------------------\r\n");
}

function afficherItems($items) {
    foreach($items as $item) {
        afficherItem($item);
    }
}

echo("=== Exercice 1 ===\r\n");
include('./ex1.php');
echo("=== Exercice 1 terminé, passage à l'exercice 2 ===\r\n");
readline("Appuyez sur Entrée\r\n");
include('./ex2.php');
echo("=== Exercice 2 terminé, passage à l'exercice 3 ===\r\n");
readline("Appuyez sur Entrée\r\n");
include('./ex3.php');
echo("=== Exercice 3 terminé, passage à l'exercice 4 ===\r\n");
readline("Appuyez sur Entrée\r\n");
include('./ex4.php');
echo("=== FIN DU SCRIPT ===");

?>