<?php
//Require du main.php pour la config
require_once('main.php');
//Alias des Modèles et Exceptions
use gestionapp\model as Model;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

// === 1. ===
echo("==============================================================================\r\n");
echo ("Question 1 : Lister les items de la commande '000b2a0b-d055-4499-9c1b-84441a254a36' :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

$commande = Model\Commande::where('id', '=', '000b2a0b-d055-4499-9c1b-84441a254a36')->with('items')->first();
afficherCommande($commande);
echo("Items de la commande n°".$commande->id."\r\n");
echo("--------------------------------------------------------\r\n");
afficherItems($commande->items);

// === 2. ===
echo("==============================================================================\r\n");
echo ("Question 2 : Lister tous les items, et pour chaque item, la liste des commandes dans lesquelles il apparaît :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

$items = Model\Item::with('commandes')->get();
foreach($items as $item) {
    afficherItem($item);
    echo("Commandes dans lesquelles l'item apparaît :\r\n");
    echo("--------------------------------------------------------\r\n");
    afficherCommandes($item->commandes);
}

// === 3. ===
echo("==============================================================================\r\n");
echo ("Question 3 : Liste des commandes passées par Aaron McGlynn et la liste des items associée, ainsi que la quantité de chacun\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

$commandes = Model\Commande::where('nom_client', '=', 'Aaron McGlynn')->with('items')->get();

foreach($commandes as $commande) {
    afficherCommande($commande);
    echo("Items de la commande n°".$commande->id."\r\n");
    echo("--------------------------------------------------------\r\n");
    afficherItems($commande->items);
}

// === 4. ===
echo("==============================================================================\r\n");
echo ("Question 4 : Dans une des commandes crées dans l'exercice 2, ajouter les items 2 et 6 avec les quantités 3 et 4\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

$date = date("Y-m-d", time());
$commande = Model\Commande::where('created_at', 'like', $date."%")->first();

$commande->items()->attach([
    2=>["quantite" => 3],
    6=>["quantite" => 4]
]);
echo("Items et quantites ajoutés à la commande avec succès\r\n");

// === 5. ===
echo("==============================================================================\r\n");
echo ("Question 5 : Dans une des commandes crées dans l'exercice 2, ajouter les items 2 et 6 avec les quantités 3 et 4\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

$commande->items()->sync([
    2=>["quantite" => 3],
    6=>["quantite" => 7]
]);
echo("Quantité de l'item 6 modifiée avec succès\r\n");