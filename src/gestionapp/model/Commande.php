<?php
namespace gestionapp\model;
use \Illuminate\Database\Eloquent\Model as EloquentModel;

class Commande extends EloquentModel {

    protected $table = 'commande';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;
    public $keyType = "string";

    public function carte() {
        return $this->belongsTo('gestionapp\model\Carte');
    }

    public function items() {
        return $this->belongsToMany('gestionapp\model\Item', 'item_commande', 'commande_id', 'item_id')->withPivot(['quantite']);
    }
}