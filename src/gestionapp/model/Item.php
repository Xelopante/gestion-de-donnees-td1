<?php
namespace gestionapp\model;
use \Illuminate\Database\Eloquent\Model as EloquentModel;

class Item extends EloquentModel {

    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function commandes() {
        return $this->belongsToMany('gestionapp\model\Commande', 'item_commande', 'item_id', 'commande_id')->withPivot(['quantite']);
    }
}