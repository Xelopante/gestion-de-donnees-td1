<?php
namespace gestionapp\model;
use \Illuminate\Database\Eloquent\Model as EloquentModel;

class Paiement extends EloquentModel {

    protected $table = 'paiement';
    protected $primaryKey = 'ref_paiement';
    public $timestamps = false;
    public $incrementing = false;
    public $keyType = "string";
}