<?php
namespace gestionapp\model;
use \Illuminate\Database\Eloquent\Model as EloquentModel;

class Carte extends EloquentModel {

    protected $table = 'carte';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function commandes() {
        return $this->hasMany('gestionapp\model\Commande', 'carte_id');
    }
}