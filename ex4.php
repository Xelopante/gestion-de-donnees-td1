<?php
//Require du main.php pour la config
require_once('main.php');
//Alias des Modèles et Exceptions
use gestionapp\model as Model;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

// === 1. ===
echo("==============================================================================\r\n");
echo ("Question 1 : Pour la carte dont le mail proprietaire contient 'Aaron.McGlynn', lister les 
commandes dont l'état est > 0 :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Récupération de la carte par le mail
$carte = Model\Carte::where("mail_proprietaire", "like", "%Aaron.McGlynn%")->first();
//Récupération des commandes associées à la carte avec un état > 0
$commandes = $carte->commandes()
                    ->where('etat', '>', 0)
                    ->get();
//Affichage
afficherCommandes($commandes);

// === 2. ===
echo("==============================================================================\r\n");
echo ("Question 2 : Lister les commandes associées à la carte 28, dont l'état est >= 0
dont le tarif est < 20 :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Récupération des commandes avec les clauses where
$commandes = Model\Commande::where([
    ['carte_id', '=', 28],
    ['etat', '>=', 0],
    ['montant', '>', 20]
])->get();
//Affichage
afficherCommandes($commandes);

// === 3. ===
echo("==============================================================================\r\n");
echo ("Question 3 : Lister les items de la commande '9f1c3241-958a-4d35-a8c9-19eef6a4fab3'
dont le tarif est < 5 :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Récupération des items avec clause whereHas sur l'id de la commande
$items = Model\Item::whereHas('commandes', function($com) {
                            $com->where('id', '=', '9f1c3241-958a-4d35-a8c9-19eef6a4fab3');
                        })->where('tarif', '<', 5)
                    ->get();
//Affichage
afficherItems($items);

// === 4. ===
echo("==============================================================================\r\n");
echo ("Question 4 : Lister les cartes qui ont été utilisées pour plus de 8
commandes :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Recherche des cartes qui comptent plus de 8 commandes
$cartes = Model\Carte::whereHas('commandes', function($commande) {
                            $commande->groupBy('carte_id')
                               ->havingRaw('count(carte_id) > ?', [8]);
})->get();

//Affichage
afficherCartes($cartes);

// === 5. ===
echo("==============================================================================\r\n");
echo ("Question 5 : Lister les cartes qui ont des commandes contenant plus de 3 items
commandes :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

$cartes = Model\Carte::whereHas('commandes', function($commande) {
    $commande->whereHas('items', function($item) {
        $item->groupBy('commande_id')
             ->havingRaw('count(commande_id) > ?', [3]);
    });
})->get();

//Affichage
afficherCartes($cartes);

// === 6. ===
echo("==============================================================================\r\n");
echo ("Question 6 : Lister les commandes qui contiennent l'item n°2
commandes :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

$commandes = Model\Commande::select('id', 'date_livraison', 'montant', 'etat')
                           ->whereHas('items', function($item) {
                                $item->where('item_id', '=', 2)
                                     ->groupBy('commande_id');
                            })
                           ->get();

//Affichage
afficherCommandes($commandes);

// === 7. ===
echo("==============================================================================\r\n");
echo ("Question 7 : Lister les cartes contenant des commandes dont le montant est > 30.0
commandes :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

$cartes = Model\Carte::whereHas('commandes', function($commande) {
                                $commande->having('montant', '>', 30);
                     })
                     ->groupBy('nom_proprietaire')
                     ->get();

//Affichage
afficherCartes($cartes);

// === 8. ===
echo("==============================================================================\r\n");
echo ("Question 8 : Lister les commandes associées à une carte et ayant + 3 items
commandes :\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

$commandes = Model\Commande::select('id', 'date_livraison', 'montant', 'etat')
                           ->where('carte_id', '!=', 'null')
                           ->whereHas('items', function($item) {
                                $item->groupBy('commande_id')
                                     ->havingRaw('count(commande_id) > ?', [3]);
                            })
                           ->get();

//Affichage
afficherCommandes($commandes);