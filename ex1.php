<?php
//Require du main.php pour la config
require_once('main.php');
//Alias des Modèles et Exceptions
use gestionapp\model as Model;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;  

// === 1. ===
echo("==============================================================================\r\n");
echo ("Question 1 : Liste des cartes de fidélité\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Récupération des cartes
$cartes = Model\Carte::get(); 
//Affichage
afficherCartes($cartes);


// === 2. ===
echo("==============================================================================\r\n");
echo ("Question 2 : Liste des cartes de fidélité (ordonnées par nom décroissant)\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Récupération de la liste des cartes avec le nom du propriétaire en ordre décroissant
$cartes = Model\Carte::orderBy('nom_proprietaire', 'desc')->get();
//Affichage
afficherCartes($cartes);


// === 3. ===
echo("==============================================================================\r\n");
echo ("Question 3 : Carte de fidélité avec l'id 7342\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Récupération de la carte avec un ID donné, avec try/catch pour gérer l'exception si la carte n'existe pas
try {
    $carte = Model\Carte::findOrFail(7342);
}
catch(ModelNotFoundException $e) {
    echo "La carte n'existe pas\r\n";
}




// === 4. ===
echo("==============================================================================\r\n");
echo ("Question 4 : Cartes de fidélité dont le nom contient 'Ariane'\r\n\r\n");
readline("Appuyez sur Entrée\r\n");
//Récupération des cartes qui comportent Ariane dans leur nom
$cartes = Model\Carte::where('nom_proprietaire', 'like', '%Ariane%')->get();
//Affichage
afficherCartes($cartes);


// === 5. ===
echo("==============================================================================\r\n");
echo ("Question 5 : Créer une nouvelle carte de fidélité\r\n\r\n");
readline("Appuyez sur Entrée\r\n");
//Appel de la fonction de création de carte avec les attributs en paramètre, l'objet est inséré dans la fonction

$carte = new Model\Carte();
$carte->password = password_hash("0550002D", PASSWORD_DEFAULT);
$carte->nom_proprietaire = "Lucas Chevalot";
$carte->mail_proprietaire = "lucas.chevalot@gmail.com";
$carte->cumul = 500;
$carte->save();
echo("Carte créée avec succès !\r\n");
    

echo("==============================================================================\r\n");

?>