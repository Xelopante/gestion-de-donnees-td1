<?php
//Require du main.php pour la config
require_once('main.php');
//Alias des Modèles et Exceptions
use gestionapp\model as Model;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

// === 1. ===
echo("==============================================================================\r\n");
echo ("Question 1 : Carte n°42 et ses commandes associées\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Affichage de la carte
$carte = Model\Carte::find(42);
afficherCarte($carte);

//Affichage des commandes associées à la carte
$commandes = $carte->commandes()->get();
echo(" === Commandes :\r\n");
afficherCommandes($commandes);

// === 2. ===
echo("==============================================================================\r\n");
echo ("Question 2 : Cartes dont le cumul > 1000, et les commandes associées de chacune d'entre elles\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Récupération des cartes dont le cumul est supérieur à 1000
$cartes = Model\Carte::where('cumul', '>', 1000)->get();
//Affichage
foreach($cartes as $carte) {
    afficherCarte($carte);
    $commandes = $carte->commandes()->get();
    echo("Commandes associées à la carte :\r\n");
    echo("--------------------------------------------------------\r\n");
    afficherCommandes($commandes);
}

// === 3. ===
echo("==============================================================================\r\n");
echo ("Question 3 : Commandes associées à une carte, et pour chacune d'entre elles leurs informations\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Récupération de toutes les commandes associées à une carte
$commandes = Model\Commande::where('carte_id', '!=', 'null')->get();
//Bouclage sur les commandes
foreach($commandes as $commande) {
    //Récupération et affichage des infos de la carte pour chaque commande
    $carte = $commande->carte()->first();
    afficherCommande($commande);
    echo("Carte associée à la commande :\r\n");
    echo("--------------------------------------------------------\r\n");
    afficherCarte($carte);
}

// === 4. ===
echo("==============================================================================\r\n");
echo ("Question 4 : Création de 3 commandes associées à la carte n°10\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Bouclage de 0 à 3 pour créer 3 commandes
for($i = 0; $i<3; $i++) {
    $commande = new Model\Commande();
    //Génération d'un id aléatoire en héxadécimal
    $id = bin2hex(openssl_random_pseudo_bytes(18));
    $commande->id = $id;
    //Récupération de la date du jour pour la livraison
    $commande->date_livraison = date("Y-m-d H:i:s", time());
    //Génération d'un montant aléatoire
    $montant = random_int(20, 40);
    $commande->montant = $montant;
    $commande->etat = 0;
    $commande->carte_id = 10;
    //Création de la commande
    $commande->save();
    echo("Commande n°".$id." créée avec succès\r\n");
}

// === 5. ===
echo("==============================================================================\r\n");
echo ("Question 5 : Changer la carte associée à la 3ème commande\r\n\r\n");
readline("Appuyez sur Entrée\r\n");

//Récupération de la dernière commande
$commande = Model\Commande::orderBy('created_at', 'desc')->first();
//Modification du numéro de la carte et sauvegarde
$commande->carte_id = 11;
$commande->save();
echo("Commande n°".$commande->id." associée à la carte n°".$commande->carte_id." avec succès\r\n");

echo("==============================================================================\r\n");

?>